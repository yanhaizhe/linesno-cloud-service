package com.alinesno.cloud.compoment.code.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.common.core.services.IBaseService;
import com.alinesno.cloud.compoment.code.entity.FrameworksTemplateEntity;
import com.alinesno.cloud.compoment.code.repository.FrameworksTemplateRepository;

/**
 * <p> 框架配置文件模板 服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@NoRepositoryBean
public interface IFrameworksTemplateService extends IBaseService<FrameworksTemplateRepository, FrameworksTemplateEntity, String> {

}
