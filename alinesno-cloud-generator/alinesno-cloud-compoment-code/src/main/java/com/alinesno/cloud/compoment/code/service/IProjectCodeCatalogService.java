package com.alinesno.cloud.compoment.code.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.common.core.services.IBaseService;
import com.alinesno.cloud.compoment.code.entity.ProjectCodeCatalogEntity;
import com.alinesno.cloud.compoment.code.repository.ProjectCodeCatalogRepository;

/**
 * <p> 生成源码资料 服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@NoRepositoryBean
public interface IProjectCodeCatalogService extends IBaseService<ProjectCodeCatalogRepository, ProjectCodeCatalogEntity, String> {

}
