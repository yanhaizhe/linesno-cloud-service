package com.alinesno.cloud.operation.cmdb.web.api;
/**
 * 接口请求的api设置
 * @author LuoAnDong
 *
 */
@java.lang.annotation.Target(value={java.lang.annotation.ElementType.TYPE})
@java.lang.annotation.Retention(value=java.lang.annotation.RetentionPolicy.RUNTIME)
@java.lang.annotation.Documented
public @interface ApiAnnotation  {
	
	/**
	 * 	数据请求的路径
	 * @return
	 */
    public String path() default "";
   
    /**
     * 版本号
     * @return
     */
    public String version() default "" ; 
    
    /**
     * 请求数据类型
     * @return
     */
    public ApiRequestMethod method() default ApiRequestMethod.HTTP ;
    
    /**
     * 接口描述
     * @return
     */
    public String desc() default ""; 
   
    /**
     * 返回类型
     * @return
     */
    public ReturnType back() default ReturnType.JSON ; 
    
    /**
     * 接口是否需要权限验证
     * @return
     */
    public boolean validate() default false ; 
    
}