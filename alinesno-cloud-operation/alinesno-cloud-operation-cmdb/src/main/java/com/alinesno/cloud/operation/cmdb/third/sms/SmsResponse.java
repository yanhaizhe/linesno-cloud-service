package com.alinesno.cloud.operation.cmdb.third.sms;
import java.io.Serializable;
import java.util.Map;

/**
 * 短信返回对象 
 * @author LuoAnDong
 * @since 2018年3月3日 上午9:22:38
 */
@SuppressWarnings("serial")
public class SmsResponse implements Serializable {
	
	private String code ; //发送返回状态码
	private String desc ; //发送返回描述 
	private String bizId ; //业务id
	private Map<String , Object> params; //参数列表  
	
	public Map<String, Object> getParams() {
		return params;
	}
	public void setParams(Map<String, Object> params) {
		this.params = params;
	}
	public String getBizId() {
		return bizId;
	}
	public void setBizId(String bizId) {
		this.bizId = bizId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}

}