package com.alinesno.cloud.operation.cmdb.third.wechat;

import java.util.List;

import com.alinesno.cloud.operation.cmdb.entity.UserEntity;

import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.bean.result.WxMpUser;

public interface WechatService {

	/**
	 * 发送消息给下单用户
	 */
	void sendTaskReceive(String orderId);

	/**
	 * 生成新订单,发送消息给业务员<br/>
	 * <b>思路</b><br/>
	 * 1. 通过多线程推送消息至业务员<br/>
	 *
	 * @param orderId
	 * @param string2 
	 * @param string 
	 */
	void sendTaskMessage(String orderId, String masterCode);

	/**
	 * 生成新订单,发送消息给业务员<br/>
	 * <b>思路</b><br/>
	 * 1. 通过多线程推送消息至业务员<br/>
	 *
	 * @param orderId
	 */
	void sendTaskMessageToSchool(String orderId, String salaId, String masterCode,  String orderUserId ,  boolean isAll);

	/**
	 * 发送通知至申请人员
	 * @param u 申请用户
	 * @param b true通过|false不通过
	 */
	void sendNoticeToApplyMan(UserEntity u, boolean b);

	/**
	 * 校内群发信息
	 * @param orderId
	 */
	void sendTaskMessageToSchoolAllPerson(String orderId, String scope);

	/**
	 * 发送订单给业务员
	 * @param orderId
	 * @param object
	 * @param masterCode
	 */
	void sendTaskMessageToSchool(String orderId, String salaId, String masterCode);

	/**
	 * 获取用户信息
	 * @param openId
	 * @return
	 * @throws WxErrorException 
	 */
	WxMpUser userInfo(String openId) throws WxErrorException;

	/**
	 * 获取用户列表
	 * @return
	 */
	List<UserEntity> userList(String nextOpenId) ; 

}