package com.alinesno.cloud.operation.cmdb.web.bean;

import com.alinesno.cloud.operation.cmdb.entity.UserEntity;

/**
 * 实体扩展
 * @author LuoAnDong
 * @since 2018年8月22日 下午6:34:24
 */
@SuppressWarnings("serial")
public class UserEntityExt extends UserEntity {
	
	private String fieldPropLabel ; //属性名称 

	public String getFieldPropLabel() {
		return fieldPropLabel;
	}

	public void setFieldPropLabel(String fieldPropLabel) {
		this.fieldPropLabel = fieldPropLabel;
	}

}
