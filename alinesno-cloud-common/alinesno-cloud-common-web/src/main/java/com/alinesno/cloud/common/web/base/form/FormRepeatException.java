package com.alinesno.cloud.common.web.base.form;

/**
 * 表单重复提交异常
 * @author LuoAnDong
 * @since 2019年2月8日 上午10:42:30
 */
@SuppressWarnings("serial")
public class FormRepeatException extends RuntimeException {

    public FormRepeatException(String message){ super(message);}
    public FormRepeatException(String message, Throwable cause){ super(message, cause);}
    
}