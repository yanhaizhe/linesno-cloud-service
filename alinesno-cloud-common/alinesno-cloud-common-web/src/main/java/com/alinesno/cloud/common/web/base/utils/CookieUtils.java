package com.alinesno.cloud.common.web.base.utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * cookies 基本操作类
 * 
 * @author LuoAnDong
 * @since 2018年4月10日 上午8:07:17
 */
@Controller
public class CookieUtils {

//	@Value("${application.cookies.max.age}")
	public int COOKIE_MAX_AGE = 3600 ; // 生命周期(默认1年）

//	@Value("${application.cookies.half.hour}")
	public int COOKIE_HALF_HOUR = 1 ;

	@Autowired
	private HttpServletRequest request;

	/**
	 * 根据Cookie名称得到Cookie对象，不存在该对象则返回Null
	 * 
	 * @param request
	 * @param name
	 * @return
	 */
	public Cookie getCookie(String name) {
		Cookie[] cookies = request.getCookies();
		if (cookies == null || cookies.length == 0) {
			return null;
		}
		Cookie cookie = null;
		for (Cookie c : cookies) {
			if (name.equals(c.getName())) {
				cookie = c;
				break;
			}
		}
		return cookie;
	}

	/**
	 * 根据Cookie名称直接得到Cookie值
	 * 
	 * @param request
	 * @param name
	 * @return
	 */
	public String getCookieValue(HttpServletRequest request, String name) {
		Cookie cookie = getCookie(name);
		if (cookie != null) {
			return cookie.getValue();
		}
		return null;
	}

	/**
	 * 移除cookie
	 * 
	 * @param request
	 * @param response
	 * @param name
	 *            这个是名称，不是值
	 */
	public void removeCookie(HttpServletRequest request, HttpServletResponse response, String name) {
		if (null == name) {
			return;
		}
		Cookie cookie = getCookie(name);
		if (null != cookie) {
			cookie.setPath("/");
			cookie.setValue("");
			cookie.setMaxAge(0);
			response.addCookie(cookie);
		}
	}

	/**
	 * 添加一条新的Cookie，可以指定过期时间(单位：秒)
	 * 
	 * @param response
	 * @param name
	 * @param value
	 * @param maxValue
	 */
	public void setCookie(HttpServletResponse response, String name, String value, int maxValue) {
		if (StringUtils.isBlank(name)) {
			return;
		}
		if (null == value) {
			value = "";
		}
		Cookie cookie = new Cookie(name, value);
		cookie.setPath("/");
		if (maxValue != 0) {
			cookie.setMaxAge(maxValue);
		} else {
			cookie.setMaxAge(COOKIE_HALF_HOUR);
		}
		response.addCookie(cookie);
		try {
			response.flushBuffer();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 添加一条新的Cookie，默认30分钟过期时间
	 * 
	 * @param response
	 * @param name
	 * @param value
	 */
	public void setCookie(HttpServletResponse response, String name, String value) {
		setCookie(response, name, value, COOKIE_HALF_HOUR);
	}

	/**
	 * 将cookie封装到Map里面
	 * 
	 * @param request
	 * @return
	 */
	public Map<String, Cookie> getCookieMap(HttpServletRequest request) {
		Map<String, Cookie> cookieMap = new HashMap<String, Cookie>();
		Cookie[] cookies = request.getCookies();
		if (cookies != null && cookies.length != 0) {
			for (Cookie cookie : cookies) {
				cookieMap.put(cookie.getName(), cookie);
			}
		}
		return cookieMap;
	}

	/**
	 * 获取全路径 
	 * @param request2
	 * @return
	 */
	public String fullUrl(HttpServletRequest request2) {
		String url = "http://" + request.getServerName() // 服务器地址
				+ ":" + request.getServerPort() // 端口号
				+ request.getContextPath() // 项目名称
				+ request.getServletPath(); // 请求页面或其他地址

		if (StringUtils.isNotBlank(request.getQueryString())) {
			url += "?" + (request.getQueryString()); // 参数
		}
		return url;
	}

}