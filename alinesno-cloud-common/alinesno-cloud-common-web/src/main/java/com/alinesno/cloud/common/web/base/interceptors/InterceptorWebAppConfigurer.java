package com.alinesno.cloud.common.web.base.interceptors;

import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 * 拦截器配置
 * 
 * @author LuoAnDong
 * @since 2018年8月14日 上午8:16:44
 */
//@Configuration
public class InterceptorWebAppConfigurer extends WebMvcConfigurationSupport {

//	@Autowired
//	private RuningInterceptor runingInterceptor;
//
//	@Autowired
//	private ManagerSessionInterceptor managerInterceptor;
//
//	@Autowired
//	private PageSessionInterceptor pageSessionInterceptor;
//	
//	private String[] staticPath = new String[] {"**.png","**.txt","**.ico","**.css","/static/**"} ; 

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		// 多个拦截器组成一个拦截器链
		// addPathPatterns 用于添加拦截规则
		// excludePathPatterns 用户排除拦截

//		if (!wechatDev) {
//			registry.addInterceptor(pageSessionInterceptor).addPathPatterns("/**")
//					.excludePathPatterns(new String[] {"/manager/**", "/wechat_token", "/wx/auth/path", "/wx/auth/callback" })
//					.excludePathPatterns(staticPath);
//		}
//
//		registry.addInterceptor(runingInterceptor).addPathPatterns("/**")
//				.excludePathPatterns(new String[] { "/run_stop", "/user_status" , "/manager/**", "/wechat_token" })
//				.excludePathPatterns(staticPath);
//
//		registry.addInterceptor(managerInterceptor).addPathPatterns("/manager/**")
//				.excludePathPatterns(new String[] { "/manager/login", "/manager/validate" })
//				.excludePathPatterns(staticPath);

		super.addInterceptors(registry);
	}

	@Override
	protected void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
		super.addResourceHandlers(registry);
	}

}