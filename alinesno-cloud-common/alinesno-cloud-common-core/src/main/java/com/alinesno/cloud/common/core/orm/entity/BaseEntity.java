package com.alinesno.cloud.common.core.orm.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.DiscriminatorOptions;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 实体对象基类,定义基本属性
 * 
 * @author LuoAnDong
 * @date 2017年8月4日
 */
@EntityListeners(AuditingEntityListener.class)
@DiscriminatorColumn(name="tenantId")  // 添加多租户支持
@DiscriminatorOptions(force=true)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@MappedSuperclass
public class BaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "snowFlakeId")
	@GenericGenerator(name = "snowFlakeId", strategy = "com.alinesno.cloud.common.core.orm.id.SnowflakeId")
	@Column(unique = true, nullable = false, length = 36)
	private String id; // 唯一ID号
	private String fieldProp; // 字段属性

	@CreatedDate
	private Date addTime; // 添加时间
	
	private Date deleteTime; // 删除时间

	@LastModifiedDate
	private Date updateTime; // 更新时间

	private Integer hasDelete = 0 ; // 是否删除(1删除|0正常|null正常)
	private Integer hasStatus = 0 ; // 状态(0启用|1禁用)
	private String deleteManager; // 删除的人

	//////////////////////////////// 数据权限规划 _start ///////////////////////
	@Column(length = 36)
	private String applicationId = "0" ;  //所属应用 应用权限: 只能看到所属应用的权限【默认】
	
	@Column(length = 36)
	private String tenantId = "0" ; //所属租户 , 租户权限 
	
	@Column(length = 36)
	private String operatorId ; // 操作员  用户权限: 只能看到自己操作的数据
	
	@Column(length = 36)
	private String fieldId ; // 字段权限：部分字段权限无法加密或者不显示，或者大于某个值
	
	@Column(length = 36)
	private String departmentId ; // 部门权限: 只能看到自己所在部门的数据
	/////////////////////////////// 数据权限规划 _end ///////////////////////
	
	public String getApplicationId() {
		return applicationId;
	}

	public String getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}

	public String getFieldId() {
		return fieldId;
	}

	public void setFieldId(String fieldId) {
		this.fieldId = fieldId;
	}

	public String getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}

	public void setHasDelete(Integer hasDelete) {
		this.hasDelete = hasDelete;
	}

	public void setHasStatus(Integer hasStatus) {
		this.hasStatus = hasStatus;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public int getHasStatus() {
		return hasStatus;
	}

	public void setHasStatus(int hasStatus) {
		this.hasStatus = hasStatus;
	}

	public String getDeleteManager() {
		return deleteManager;
	}

	public void setDeleteManager(String deleteManager) {
		this.deleteManager = deleteManager;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFieldProp() {
		return fieldProp;
	}

	public void setFieldProp(String fieldProp) {
		this.fieldProp = fieldProp;
	}

//	public int isHasDelete() {
//		return hasDelete;
//	}
//
//	public void setHasDelete(int hasDelete) {
//		this.hasDelete = hasDelete;
//	}
	
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getAddTime() {
		return addTime;
	}

	public int getHasDelete() {
		return hasDelete;
	}

	public void setHasDelete(int hasDelete) {
		this.hasDelete = hasDelete;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getDeleteTime() {
		return deleteTime;
	}

	public void setDeleteTime(Date deleteTime) {
		this.deleteTime = deleteTime;
	}

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

}
