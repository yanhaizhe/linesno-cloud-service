package com.alinesno.cloud.common.core.rest;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.alibaba.fastjson.JSONObject;
import com.alinesno.cloud.common.core.orm.entity.BaseEntity;
import com.alinesno.cloud.common.core.services.IBaseService;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;

/**
 * 对外rest接口基类
 *
 * @author LuoAnDong
 * @since 2018年11月21日 上午10:46:56
 */
public abstract class BaseRestController<E extends BaseEntity, S extends IBaseService<?, E, String>> extends SuperRestController {

	protected static final Logger log = LoggerFactory.getLogger(BaseRestController.class);

	@Autowired
	protected S feign;
	
	
	@GetMapping("findAll")
	public List<E> findAll() {
		return feign.findAll();
	}

	@PostMapping("findAllBySort")
	public List<E> findAll(@RequestBody Sort sort) {
		return feign.findAll(sort);
	}

	@PostMapping("findAllById")
	public List<E> findAllById(@RequestBody Iterable<String> ids) {
		log.debug("ids = {}", ids);
		return feign.findAllById(ids);
	}

	@PostMapping("saveAll")
	public List<E> saveall(@RequestBody Iterable<E> entities) {
		return feign.saveAll(entities);
	}

	@PostMapping("saveAndFlush")
	public E saveAndFlush(@RequestBody E entity) {
		return feign.saveAndFlush(entity);
	}

	@PostMapping("deleteInBatch")
	public void deleteInBatch(@RequestBody Iterable<E> entities) {
		feign.deleteInBatch(entities);
	}

	@GetMapping("deleteAllInBatch")
	public void deleteAllInBatch() {
		feign.deleteAllInBatch();
	}

	@GetMapping("getOne")
	public E getOne(String id) {
		log.debug("id = {}", id);
		return feign.getOne(id);
	}

	@PostMapping(value="findAllByPageable" , consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Page<E> findAll(@RequestParam("page") Pageable pageable) {

		log.debug("findAll: {}", ToStringBuilder.reflectionToString(pageable));

		Page<E> page = feign.findAll(pageable) ; //PageRequest.of(restPage.getNumber(), restPage.getSize() , restPage.getSort()));

		log.debug("page: {}", ToStringBuilder.reflectionToString(page));
		log.debug("content: {}", ToStringBuilder.reflectionToString(page.getContent()));

		for (E e : page.getContent()) {
			log.debug("e: {}", ToStringBuilder.reflectionToString(e));
		}

		return page ;
	}

	@PostMapping("save")
	public E save(@RequestBody E entity) {
		return feign.save(entity);
	}

	@GetMapping("findById")
	public Optional<E> findById(String id) {
		return feign.findById(id);
	}

	@GetMapping("existsById")
	public boolean existsById(String id) {
		return feign.existsById(id);
	}

	@GetMapping("count")
	public long count() {
		return feign.count();
	}

	@GetMapping("deleteById")
	public void deleteById(String id) {
		feign.deleteById(id);
	}

	@PostMapping("deleteByIds")
	public void deleteByIds(@RequestBody String[] ids) {
		feign.deleteByIds(ids) ;
	}

	@PostMapping("delete")
	public void delete(@RequestBody E entity) {
		feign.delete(entity);
	}

	@PostMapping("deleteAllByIterable")
	public void deleteAll(@RequestBody Iterable<E> entities) {
		feign.deleteAll(entities);
	}

	@PostMapping("deleteAll")
	public void deleteAll() {
		feign.deleteAll();
	}

	@PostMapping(value="findOneByWrapper", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Optional<E> findOne(@RequestBody RestWrapper restWrapper) {
		log.debug("findOne: {}", ToStringBuilder.reflectionToString(restWrapper));
		return feign.findOne(SpecificationBuilder(restWrapper));
	}

	@PostMapping(value="findAllByWrapper", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<E> findAll(@RequestBody RestWrapper restWrapper) {
		log.debug("findAll: {}", ToStringBuilder.reflectionToString(restWrapper));
		return feign.findAll(SpecificationBuilder(restWrapper));
	}

	@PostMapping(value="findAllByWrapperAndPageable" , consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE )
	public Page<E> findAllByWrapperAndPageable(@RequestBody RestWrapper restWrapper) {
		log.debug("===> findAllByWrapperAndPageable:{}", JSONObject.toJSON(restWrapper));

		Pageable pageable = PageRequest.of(restWrapper.getPageNumber(), restWrapper.getPageSize()) ;
//		Page<E> page = feign.findAll(SpecificationBuilder(restWrapper), PageRequest.of(restPage.getNumber(), restPage.getSize(), restPage.getSort()));
//		return new RestPage<E>(page);
		
		Page<E> page = feign.findAll(SpecificationBuilder(restWrapper), pageable);
		return page ;
	}

	/**
	 * 更新实体状态
	 * @param id
	 * @return
	 */
	@GetMapping("modifyHasStatus")
	boolean modifyHasStatus(@RequestParam("id") String id) {
		return feign.modifyHasStatus(id);
	}


	@GetMapping("findAllByApplicationId")
	List<E> findAllByApplicationId(@RequestParam("applicationId") String applicationId){
		return feign.findAllByApplicationId(applicationId) ;
	}

	@GetMapping("findAllByTenantId")
	List<E> findAllByTenantId(@RequestParam("tenantId") String tenantId){
		return feign.findAllByTenantId(tenantId) ;
	}
}
