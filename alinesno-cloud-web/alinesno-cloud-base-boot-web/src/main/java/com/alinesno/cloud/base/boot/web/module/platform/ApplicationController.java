package com.alinesno.cloud.base.boot.web.module.platform;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.base.boot.feign.dto.ManagerApplicationDto;
import com.alinesno.cloud.base.boot.feign.dto.ManagerResourceDto;
import com.alinesno.cloud.base.boot.feign.dto.ManagerRoleResourceDto;
import com.alinesno.cloud.base.boot.feign.facade.ManagerApplicationFeigin;
import com.alinesno.cloud.base.boot.feign.facade.ManagerResourceFeigin;
import com.alinesno.cloud.base.boot.feign.facade.ManagerRoleResourceFeigin;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.bean.DatatablesPageBean;
import com.alinesno.cloud.common.web.base.controller.FeignMethodController;
import com.alinesno.cloud.common.web.base.form.FormToken;
import com.alinesno.cloud.common.web.base.response.ResponseBean;
import com.alinesno.cloud.common.web.base.response.ResponseGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import cn.hutool.core.bean.BeanUtil;

/**
 * 应用管理
 * 
 * @author LuoAnDong
 * @since 2018年12月7日 下午10:58:20
 */
@Controller
@RequestMapping("boot/platform/application")
public class ApplicationController extends FeignMethodController<ManagerApplicationDto , ManagerApplicationFeigin> {

	private static final Logger log = LoggerFactory.getLogger(ApplicationController.class);

	@Autowired
	private ManagerApplicationFeigin managerApplicationFeigin ; 

	@Autowired
	private ManagerRoleResourceFeigin managerRoleResourceFeigin ; 
	
	@Autowired
	private ManagerResourceFeigin managerResourceFeigin ; 
	
	/**
	 * 菜单管理查询功能  
	 * @return
	 */
	@GetMapping("/list")
    public void list(){
    }
	
	/**
	 * 树查询
	 */
	@GetMapping("/select")
    public void select(){
		log.debug("进入树列表选择页面.");
    }
	
	@GetMapping("/menus")
    public void menus(Model model , String id){
		log.debug("application id = {}" , id);
		model.addAttribute("applicationId", id) ; 
    }

	@ResponseBody
	@PostMapping("/menusData")
    public List<JsonNode> menusData(Model model , String applicationId , String roleId) throws JsonProcessingException, IOException{
		
		log.debug("applicationId:{} , roleId:{}" , applicationId , roleId);
		model.addAttribute("applicationId", applicationId) ; 
		
		List<JsonNode> nodeList = new ArrayList<JsonNode>() ; 
		List<ManagerResourceDto> list = null ; 
		
		if(StringUtils.isNotBlank(applicationId)) {
			list = managerResourceFeigin.findAllByApplicationId(applicationId) ; 
		}else {
			list = managerResourceFeigin.findAll() ; 
		}
		
		Map<String , Object> map = new HashMap<String , Object>() ; 
		
		if(StringUtils.isNotBlank(roleId)) {
			
			RestWrapper restWrapper = new RestWrapper().eq("roleId", roleId) ; 
			List<ManagerRoleResourceDto> mrList = managerRoleResourceFeigin.findAll(restWrapper) ; 
			for(ManagerRoleResourceDto b : mrList) {
				log.debug("id:{} , roleId:{}" , b.getId(), b.getRoleId());
				map.put(b.getResourceId(), b) ; 
			}
		}
		
		for(ManagerResourceDto b : list) {
			ObjectNode node = objectMapper.readValue(objectMapper.writeValueAsBytes(b), ObjectNode.class) ;
			log.debug("map.containsKey(b.getId()):{}" , map.containsKey(b.getId()));
			
			if(map.containsKey(b.getId())) {
				node.put("checked", true) ; 
			}
			nodeList.add(node) ; 
		}
		
		return nodeList ; 
    }
	
	@TranslateCode("[{hasStatus:has_status}]")
	@ResponseBody
	@PostMapping("/datatables")
    public DatatablesPageBean datatables(HttpServletRequest request , Model model ,  DatatablesPageBean page){
		log.debug("page = {}" , ToStringBuilder.reflectionToString(page));
		return this.toPage(model, managerApplicationFeigin , page) ;
    }

	/**
	 * 菜单管理查询功能  
	 * @return
	 */
	@FormToken(save=true)
	@GetMapping("/add")
    public void add(Model model , HttpServletRequest request){
    }
	
	/**
	 * 保存新对象 
	 * @param model
	 * @param managerCodeDto
	 * @return
	 */
	@FormToken(remove=true)
	@ResponseBody
	@PostMapping("/save")
	public ResponseBean save(Model model , HttpServletRequest request, ManagerApplicationDto managerApplicationDto) {
		managerApplicationDto = managerApplicationFeigin.save(managerApplicationDto) ; 
		
		return ResponseGenerator.ok(null) ; 	
	}

	/**
	 * 保存新对象 
	 * @param model
	 * @param managerCodeDto
	 * @return
	 */
	@FormToken(remove=true)
	@ResponseBody
	@PostMapping("/update")
	public ResponseBean update(Model model , HttpServletRequest request, ManagerApplicationDto managerApplicationDto) {
		
		ManagerApplicationDto dto = managerApplicationFeigin.getOne(managerApplicationDto.getId()) ; 
		BeanUtil.copyProperties(managerApplicationDto, dto, copyOptions); 
		
		managerApplicationDto = managerApplicationFeigin.save(dto) ; 
		return ResponseGenerator.ok(null) ; 	
	}
	
	/**
	 * 菜单管理查询功能  
	 * @return
	 */
	@FormToken(save=true)
	@GetMapping("/modify")
    public void modify(HttpServletRequest request ,Model model , String id){
		Assert.hasLength(id , "主键不能为空.");
		
		ManagerApplicationDto code = managerApplicationFeigin.getOne(id) ; 
		
		model.addAttribute("bean", code) ; 
    }
	
	/**
	 * 删除
	 */
	@ResponseBody
	@PostMapping("/delete")
    public ResponseBean delete(@RequestParam(value = "rowsId[]") String[] rowsId){
		log.debug("rowsId = {}" , ToStringBuilder.reflectionToString(rowsId));
		if(rowsId != null && rowsId.length > 0) {
			managerApplicationFeigin.deleteByIds(rowsId); 
		}
		return ResponseGenerator.ok(null) ; 
    }
	
	/**
	 * 所有应用数据
	 * @param rowsId
	 * @return
	 */
	@ResponseBody
	@PostMapping("/allData")
    public List<ManagerApplicationDto> allData(){
		return managerApplicationFeigin.findAll() ; 
    }
	
}
