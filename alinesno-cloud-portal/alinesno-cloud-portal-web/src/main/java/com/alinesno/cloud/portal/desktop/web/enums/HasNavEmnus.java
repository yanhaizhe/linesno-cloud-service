package com.alinesno.cloud.portal.desktop.web.enums;

/**
 * 导航枚举
 * @author LuoAnDong
 * @since 2019年5月19日 下午9:50:20
 */
public enum HasNavEmnus {

	NAV("1","主导航") , NO_NAV("0" , "非主导航") ;

	private String value ; 
	private String text ; 
	
	private HasNavEmnus(String v , String t) {
		this.value = v ; 
		this.text = t ; 
	}

	public String getText() {
		return text;
	}

	public String getValue() {
		return value;
	}
	
}
