<%@ page pageEncoding="UTF-8"%> 

<jsp:directive.include file="includes/top.jsp" />
<body data-app-state="">
	<div id="root">
		<div>
			<div class="zJwEi">
				<div class="eaYrSC">
					<div class="jCprYf"></div>
					<div class="iMoAjM">
						<div class="vHoXo">
							<header class="dMTsYF">
								<jsp:directive.include file="includes/svg.jsp" />
								<h1>单点登陆系统</h1>
							</header>
							<section role="main" class="ffnCIW">
								<div class="cMZWRp">
									<div>
										<div role="alert" class="hidden bYGriA"></div>
										<form:form method="post" id="fm1" commandName="${commandName}"
											htmlEscape="true">
											<section class="row btn-row">
												<input type="hidden" name="lt" value="${loginTicket}" /> <input
													type="hidden" name="execution" value="${flowExecutionKey}" />
												<input type="hidden" name="_eventId" value="submit" />
											</section>

											<div id="form-sign-up">
												<form:errors path="*" id="msg" cssClass="errors" cssStyle="padding:10px;" element="div" htmlEscape="false" />
												<div id="notice" class="errors" style="padding: 10px; display:none;background-color: rgb(255, 238, 221);">请先完成验证.</div>
												<div class="  hRPdFp">
													<div>
														<!-- 
														<label for="email" class="cQjUNh">
															<div class="cRWorA">
																<span>账户</span>
															</div>
														</label>
														 -->
														<div class="eexybP">
															<div>
																<div>
																	<div>
																		<div class="gnyRfH">
																			<div class="NYali">
																				<form:input cssClass="gcfMkP" cssErrorClass="error"
																					placeholder="帐号"
																					id="username" size="25" tabindex="1"
																					accesskey="${userNameAccessKey}" path="username"
																					autocomplete="off" htmlEscape="true" />
																			</div>
																		</div>
																	</div>
																</div>
																<div class="eftsfW">
																	<div style="top: 0px; left: 0px; position: absolute; z-index: 400; opacity: 0;"></div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="  hRPdFp">
													<div>
														<div class="geHwRp">
															<div>
																<!-- 
																<label for="password" class="cQjUNh">
																	<div class="cRWorA">
																		<span>密码</span>
																	</div>
																</label>
																 -->
																<div class="eexybP">
																	<div>
																		<div>
																			<div>
																				<div class="gnyRfH">
																					<div class="NYali"> 
																						<form:password cssClass="gcfMkP"
																							placeholder="密码"
																							cssErrorClass="error" id="password" size="25"
																							tabindex="2" path="password"
																							accesskey="${passwordAccessKey}"
																							htmlEscape="true" autocomplete="off" />
																					</div>
																				</div>
																			</div>
																		</div>
																		<div class="eftsfW">
																			<div
																				style="top: 0px; left: 0px; position: absolute; z-index: 400; opacity: 0;"></div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div>
														<p class="dvrYmd"></p>
													</div>
												</div>
												
												<!-- 
												<div class="hSHNUR">
													 <div id="captcha">
														<p id="wait" style="height:44px;line-height:15px;" class="show">正在加载验证码......</p>
													</div>
												</div>
												 -->
												
												<p class="signup-legal daSVgz">
													<span>登陆隐私说明 <a
														href="https://www.atlassian.com/end-user-agreement"
														target="_blank" class="end-user-agreement">同意说明</a>
													</span>
												</p>
												<div class="hSHNUR">
													<button class="koAzE ixXVkd" spacing="default" style="cursor:pointer;" id="signup-submit" type="button">
														<span style="align-self: center; display: inline-flex; flex-wrap: nowrap; max-width: 100%; width: 100%; justify-content: center;">
																	<spring:message code="screen.welcome.button.login" />
														</span>
													</button>
												</div>
											</div>
										</form:form>
									</div>
								</div>
							</section>
							<jsp:directive.include file="includes/bottom.jsp" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 引入 gt.js，既可以使用其中提供的 initGeetest 初始化函数 -->
	<script src="<%=basePath%>js/gt.js"></script>

	<script>
		$(function(){
			$("#signup-submit").click(function(e) {
				$("#fm1").submit() ; 
			}) ; 
		})
		/*
		var handler = function(captchaObj) {
			$("#signup-submit").click(function(e) {
				var result = captchaObj.getValidate();
				console.log("result = " + result) ; 
				if (!result) { 
					$("#notice").fadeIn();
					setTimeout(function() {
						$("#notice").fadeOut();
					}, 2000);
					e.preventDefault();
				}else{
					$("#fm1").submit() ; 
				}
			});
			// 将验证码加到id为captcha的元素里，同时会有三个input的值用于表单提交
			captchaObj.appendTo("#captcha");
			captchaObj.onReady(function() {
				$("#wait").hide();
			});
			// 更多接口参考：http://www.geetest.com/install/sections/idx-client-sdk.html
		};
		$.ajax({
			url : "gt/register?t=" + (new Date()).getTime(), // 加随机数防止缓存
			type : "get",
			dataType : "json",
			success : function(data) {
				initGeetest({
					gt : data.gt,
					challenge : data.challenge,
					new_captcha : data.new_captcha,
					offline : !data.success,
					product : "float",
					width : "100%"
				}, handler);
			}
		});
		*/
	</script>


</body>
</html>

