package com.alinesno.cloud.demo.activiti.config;

import org.activiti.spring.SpringProcessEngineConfiguration;
import org.activiti.spring.boot.ProcessEngineConfigurationConfigurer;
import org.springframework.stereotype.Component;

/**
 * 工作流配置
 * @author LuoAnDong
 * @since 2019年7月10日 下午12:34:26
 */
@Component
public class ActivitiConfig implements ProcessEngineConfigurationConfigurer {
	
    @Override
    public void configure(SpringProcessEngineConfiguration processEngineConfiguration) {
        processEngineConfiguration.setActivityFontName("宋体");
        processEngineConfiguration.setLabelFontName("宋体");
        processEngineConfiguration.setAnnotationFontName("宋体");
       
        /**
         * 这段代码表示是否使用activiti自带用户组织表，如果是，这里为true，如果不是，这里为false。<br/>
         * 由于本项目使用了视图的方式代替了原有的用户组织表，所以这里设置为false，这样启动就不用去检查用户组织表是否存在。
         */
        processEngineConfiguration.setDbIdentityUsed(false);
        processEngineConfiguration.setDatabaseSchemaUpdate("true");
    }
    
}