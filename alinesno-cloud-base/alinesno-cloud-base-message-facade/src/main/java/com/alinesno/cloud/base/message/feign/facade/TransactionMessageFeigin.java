package com.alinesno.cloud.base.message.feign.facade;

import java.util.List;
import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.alinesno.cloud.base.message.feign.dto.TransactionMessageDto;
import com.alinesno.cloud.base.message.feign.dto.TransactionMessageHistoryDto;
import com.alinesno.cloud.common.facade.feign.IBaseFeign;

/**
 * <p>  请求客户端 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-08 09:28:52
 */
@FeignClient(name="alinesno-cloud-base-message" , path="transactionMessage")
public interface TransactionMessageFeigin extends IBaseFeign<TransactionMessageDto> {

	/**
	 * 保存预发送消息
	 * 
	 * @param e
	 * @return 保存的消息id
	 */
	@PostMapping("saveMessageLocal")
	public String saveMessageLocal(@RequestBody TransactionMessageDto e)  ; 

	/**
	 * 确认并发送消息
	 * 
	 * @param id
	 * @return 成功true | 失败false
	 */
	@GetMapping("confirmAndSendMessage")
	public boolean confirmAndSendMessage(@RequestParam("id") String id)  ; 

	/**
	 * 保存消息并直接发送
	 * 
	 * @param e
	 * @return 成功true | 失败false @
	 */
	@PostMapping("saveAndSendMessage")
	public boolean saveAndSendMessage(@RequestBody TransactionMessageDto e) ; 

	/**
	 * 直接发送消息，不保存本地
	 * 
	 * @param e
	 * @return 成功true | 失败false @
	 */
	@PostMapping("directSendMessage")
	public boolean directSendMessage(@RequestBody TransactionMessageDto e) ;

	/**
	 * 根据消息id重新发送消息,重新发送的消息,会将消息重发次数加1
	 * 
	 * @param e
	 * @return 成功true | 失败false @
	 */
	@GetMapping("reSendMessageById")
	public boolean reSendMessageById(@RequestParam("id") String id)  ; 

	/**
	 * 根据id将消息标记为死亡状态
	 * 
	 * @param id
	 * @return @
	 */
	@GetMapping("setMessageToAreadlyDead")
	public boolean setMessageToAreadlyDead(@RequestParam("id") String id)  ; 

	/**
	 * 根据消息id获取消息,其中包括已经死亡的消息，不包括历史消息
	 * 
	 * @param id 消息的id
	 * @return 存在返回实体|不存在则返回null @
	 */
	@GetMapping("getMessageById")
	public TransactionMessageDto getMessageById(@RequestParam("id") String id)  ; 

	/**
	 * 根据消息id获取死亡的消息
	 * 
	 * @param id 消息的id
	 * @return 存在返回实体|不存在则返回null @
	 */
	@GetMapping("getDeathMessageById")
	public TransactionMessageDto getDeathMessageById(@RequestParam("id") String id)  ; 

	/**
	 * 根据消息id获取历史消息
	 * 
	 * @param id 消息的id
	 * @return 存在返回实体|不存在则返回null @
	 */
	@GetMapping("getHistoryMessageById")
	public TransactionMessageHistoryDto getHistoryMessageById(@RequestParam("id") String id)  ; 

	/**
	 * 根据队名获取消息
	 * 
	 * @param String queueName 队列的id
	 * @return @
	 */
	@GetMapping("getMessageByTopic")
	public Page<TransactionMessageDto> getMessageByTopic(
			@RequestParam("topic") String topic, 
			@RequestParam("pageNow") int pageNow, 
			@RequestParam("pageSize") int pageSize)  ; 

	/**
	 * 根据队列名和是否死亡获取消息
	 * 
	 * @param String queueName 队列的id , Map<String, Object> param
	 * @return @
	 */
	@GetMapping("getMessageByTopicAndParam")
	public Page<TransactionMessageDto> getMessageByTopicAndParam(
			@RequestParam("topic") String topic, 
			@RequestParam("params") Map<String, Object> params, 
			@RequestParam("pageNow") int pageNow, 
			@RequestParam("pageSize") int pageSize) ; 

	/**
	 * 根据消息的id删除消息,即移到历史表
	 * 
	 * @param id
	 * @return 成功true | 失败false @
	 */
	@GetMapping("deleteMessageById")
	public boolean deleteMessageById(@RequestParam("id") String id)  ; 

	/**
	 * 根据业务id确认消息已经成功消费,然后删除消息id
	 * 
	 * @param id
	 * @return @
	 */
	@GetMapping("confirmMessageByMessageId")
	public boolean confirmMessageByMessageId(@RequestParam("id") String id) ; 

	/**
	 * 重发某个消息列队中的全部死亡消息
	 * 
	 * @param queueName 列队的名称
	 * @param batchSize 发送的条数
	 * @return 成功null | 失败返回失败消息的id @
	 */
	@GetMapping("reSendAllMessageByTopic")
	public List<String> reSendAllMessageByTopic(
			@RequestParam("topic") String topic, 
			@RequestParam("batchSize") int batchSize)  ; 

	/**
	 * 分页获取消息数据
	 * 
	 * @param          <PageBean>
	 * @param params   参数
	 * @param pageNow  当前页
	 * @param pageSize 分页条数
	 * @return
	 */
	@GetMapping("listMessagePage")
	public Page<TransactionMessageDto> listMessagePage(
			@RequestParam("pageNow") int pageNow, 
			@RequestParam("pageSize") int pageSize, 
			@RequestParam("params") Map<String, Object> params) ; 

	/**
	 * 发送kafka消息
	 * 
	 * @param messageBody
	 * @param topicName
	 * @return 成功则返回true，否则失败
	 */
	@GetMapping("sendMessage")
	boolean sendMessage(
			@RequestParam("messageBody") String messageBody, 
			@RequestParam("topic") String topic) ; 
	
}
