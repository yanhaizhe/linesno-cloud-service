package com.alinesno.cloud.base.workflow.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ActivityDefineEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3236952749923057941L;
	/**
	 * 活动定义ID
	 */
	String id = "";
	/**
	 * 活动定义名称
	 */
	String name = "";
	/**
	 * 活动类型
	 */
	String type = "";
	/**
	 * 所属业务流程ID
	 */
	long processDefId;
	/**
	 * 分支模式
	 */
	String splitMode = "AND";
	/**
	 * 聚合模式
	 */
	String joinMode = "AND";
	/**
	 * 优先级
	 */
	String priority = "";
	/**
	 * 是否允许代理
	 */
	String allowAgent = "";
	/**
	 * 活动参与者类型
	 */
	String participantType = "organization-list";
	/**
	 * 活动参与者列表
	 */
	List<ParticipantBean> participants;
	/**
	 * URL类型
	 */
	String urlType = "";
	/**
	 * URLID
	 */
	String urlID = "";
	/**
	 * 
	 */
	String reStartUrlType = "";
	/**
	 * 指派参与者列表
	 */
	String reStartUrlID = "";
	/**
	 * 
	 */
	boolean isSpecifyURL = false;
	/**
	 * 
	 */
	List<ParticipantBean> appointedParticipants = new ArrayList<ParticipantBean>();
	/**
	 * 是否允许指派
	 */
	boolean allowAppoint;
	/**
	 * 是否多工作项
	 */
	boolean multiWorkItem;
	/**
	 * 
	 */
	boolean subProcess = false;
	/**
	 * 
	 */
	String subProcessDefName = "";

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public long getProcessDefId() {
		return processDefId;
	}

	public void setProcessDefId(long processDefId) {
		this.processDefId = processDefId;
	}

	public String getSplitMode() {
		return splitMode;
	}

	public void setSplitMode(String splitMode) {
		this.splitMode = splitMode;
	}

	public String getJoinMode() {
		return joinMode;
	}

	public void setJoinMode(String joinMode) {
		this.joinMode = joinMode;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getAllowAgent() {
		return allowAgent;
	}

	public void setAllowAgent(String allowAgent) {
		this.allowAgent = allowAgent;
	}

	public String getParticipantType() {
		return participantType;
	}

	public void setParticipantType(String participantType) {
		this.participantType = participantType;
	}

	public List<ParticipantBean> getParticipants() {
		return participants;
	}

	public void setParticipants(List<ParticipantBean> participants) {
		this.participants = participants;
	}

	public String getUrlType() {
		return urlType;
	}

	public void setUrlType(String urlType) {
		this.urlType = urlType;
	}

	public String getUrlID() {
		return urlID;
	}

	public void setUrlID(String urlID) {
		this.urlID = urlID;
	}

	public String getReStartUrlType() {
		return reStartUrlType;
	}

	public void setReStartUrlType(String reStartUrlType) {
		this.reStartUrlType = reStartUrlType;
	}

	public String getReStartUrlID() {
		return reStartUrlID;
	}

	public void setReStartUrlID(String reStartUrlID) {
		this.reStartUrlID = reStartUrlID;
	}

	public boolean isSpecifyURL() {
		return isSpecifyURL;
	}

	public void setSpecifyURL(boolean isSpecifyURL) {
		this.isSpecifyURL = isSpecifyURL;
	}

	public List<ParticipantBean> getAppointedParticipants() {
		return appointedParticipants;
	}

	public void setAppointedParticipants(List<ParticipantBean> appointedParticipants) {
		this.appointedParticipants = appointedParticipants;
	}

	public boolean isAllowAppoint() {
		return allowAppoint;
	}

	public void setAllowAppoint(boolean allowAppoint) {
		this.allowAppoint = allowAppoint;
	}

	public boolean isMultiWorkItem() {
		return multiWorkItem;
	}

	public void setMultiWorkItem(boolean multiWorkItem) {
		this.multiWorkItem = multiWorkItem;
	}

	public boolean isSubProcess() {
		return subProcess;
	}

	public void setSubProcess(boolean subProcess) {
		this.subProcess = subProcess;
	}

	public String getSubProcessDefName() {
		return subProcessDefName;
	}

	public void setSubProcessDefName(String subProcessDefName) {
		this.subProcessDefName = subProcessDefName;
	}

	@Override
	public String toString() {
		return "WFActivityDefine [id=" + id + ", name=" + name + ", type=" + type + ", processDefId=" + processDefId
				+ ", splitMode=" + splitMode + ", joinMode=" + joinMode + ", priority=" + priority + ", allowAgent="
				+ allowAgent + ", participantType=" + participantType + ", participants=" + participants + ", urlType="
				+ urlType + ", urlID=" + urlID + ", reStartUrlType=" + reStartUrlType + ", reStartUrlID=" + reStartUrlID
				+ ", isSpecifyURL=" + isSpecifyURL + ", appointedParticipants=" + appointedParticipants
				+ ", allowAppoint=" + allowAppoint + ", multiWorkItem=" + multiWorkItem + ", subProcess=" + subProcess
				+ ", subProcessDefName=" + subProcessDefName + "]";
	}

}
