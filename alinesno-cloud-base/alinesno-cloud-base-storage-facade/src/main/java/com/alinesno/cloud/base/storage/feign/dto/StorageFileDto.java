package com.alinesno.cloud.base.storage.feign.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2019-06-08 09:19:40
 */
@SuppressWarnings("serial")
public class StorageFileDto extends BaseDto {

	private String dfsGroupName;
	
	private Integer downloadNum;
	
	private String downloadPwd;
	
	private Date expirationDate;
	
    /**
     * 文件扩展名
     */
	private String fileExt;
	
    /**
     * 文件标识
     */
	private String fileFlag;
	
    /**
     * 文件名称
     */
	private String fileName;
	
    /**
     * 文件长度
     */
	private BigDecimal fileSize;
	
    /**
     * 地址链接
     */
	private String fileUrl;
	
    /**
     * 文件源
     */
	private String filesource;
	
    /**
     * 是否公开
     */
	private String isPublic;
	
    /**
     * 保存类型
     */
	private String saveType;
	
    /**
     * 阿里云链接
     */
	private String urlAlioss;
	
    /**
     * 本地磁盘链接
     */
	private String urlDisk;
	
    /**
     * fastdfs链接
     */
	private String urlFastdfs;
	
	private String urlPaxossurlre;
	
    /**
     * 七牛链接
     */
	private String urlQiniu;
	
	private String urlbfs;
	
	private String urlmongodb;
	


	public String getDfsGroupName() {
		return dfsGroupName;
	}

	public void setDfsGroupName(String dfsGroupName) {
		this.dfsGroupName = dfsGroupName;
	}

	public Integer getDownloadNum() {
		return downloadNum;
	}

	public void setDownloadNum(Integer downloadNum) {
		this.downloadNum = downloadNum;
	}

	public String getDownloadPwd() {
		return downloadPwd;
	}

	public void setDownloadPwd(String downloadPwd) {
		this.downloadPwd = downloadPwd;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getFileExt() {
		return fileExt;
	}

	public void setFileExt(String fileExt) {
		this.fileExt = fileExt;
	}

	public String getFileFlag() {
		return fileFlag;
	}

	public void setFileFlag(String fileFlag) {
		this.fileFlag = fileFlag;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public BigDecimal getFileSize() {
		return fileSize;
	}

	public void setFileSize(BigDecimal fileSize) {
		this.fileSize = fileSize;
	}

	public String getFileUrl() {
		return fileUrl;
	}

	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}

	public String getFilesource() {
		return filesource;
	}

	public void setFilesource(String filesource) {
		this.filesource = filesource;
	}

	public String getIsPublic() {
		return isPublic;
	}

	public void setIsPublic(String isPublic) {
		this.isPublic = isPublic;
	}

	public String getSaveType() {
		return saveType;
	}

	public void setSaveType(String saveType) {
		this.saveType = saveType;
	}

	public String getUrlAlioss() {
		return urlAlioss;
	}

	public void setUrlAlioss(String urlAlioss) {
		this.urlAlioss = urlAlioss;
	}

	public String getUrlDisk() {
		return urlDisk;
	}

	public void setUrlDisk(String urlDisk) {
		this.urlDisk = urlDisk;
	}

	public String getUrlFastdfs() {
		return urlFastdfs;
	}

	public void setUrlFastdfs(String urlFastdfs) {
		this.urlFastdfs = urlFastdfs;
	}

	public String getUrlPaxossurlre() {
		return urlPaxossurlre;
	}

	public void setUrlPaxossurlre(String urlPaxossurlre) {
		this.urlPaxossurlre = urlPaxossurlre;
	}

	public String getUrlQiniu() {
		return urlQiniu;
	}

	public void setUrlQiniu(String urlQiniu) {
		this.urlQiniu = urlQiniu;
	}

	public String getUrlbfs() {
		return urlbfs;
	}

	public void setUrlbfs(String urlbfs) {
		this.urlbfs = urlbfs;
	}

	public String getUrlmongodb() {
		return urlmongodb;
	}

	public void setUrlmongodb(String urlmongodb) {
		this.urlmongodb = urlmongodb;
	}

}
