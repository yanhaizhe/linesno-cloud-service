package com.alinesno.cloud.demo.base.feign.api;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.alinesno.cloud.demo.base.feign.dto.LoggerDto;

/**
 * 用户日志服务接口
 * @author LuoAnDong
 * @since 2018年11月21日 下午2:31:17
 */
@FeignClient("alinesno-cloud-demo-base")
public interface ILoggerFeign {

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Object> findById(@RequestParam("id") String id);

    @RequestMapping(value = "/logger/findAll", method = RequestMethod.GET)
    public ResponseEntity<Object> findAll();
    
    @RequestMapping(value = "/logger/count", method = RequestMethod.GET)
    public ResponseEntity<Long> count()  ; 
    
    @RequestMapping(value = "/hello/{name}")
    public ResponseEntity<String> hello(@PathVariable("name") String name) ;

    @PostMapping("/logger/save")
	public void save(LoggerDto log);
    
}
