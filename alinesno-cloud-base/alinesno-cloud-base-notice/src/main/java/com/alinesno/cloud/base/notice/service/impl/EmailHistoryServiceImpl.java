package com.alinesno.cloud.base.notice.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.base.notice.entity.EmailHistoryEntity;
import com.alinesno.cloud.base.notice.repository.EmailHistoryRepository;
import com.alinesno.cloud.base.notice.service.IEmailHistoryService;
import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-02 16:08:00
 */
@Service
public class EmailHistoryServiceImpl extends IBaseServiceImpl<EmailHistoryRepository, EmailHistoryEntity, String> implements IEmailHistoryService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(EmailHistoryServiceImpl.class);

}
