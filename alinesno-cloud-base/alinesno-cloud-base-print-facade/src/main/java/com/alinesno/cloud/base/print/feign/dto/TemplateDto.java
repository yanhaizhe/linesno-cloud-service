package com.alinesno.cloud.base.print.feign.dto;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2019-06-07 21:26:00
 */
@SuppressWarnings("serial")
public class TemplateDto extends BaseDto {

    /**
     * 模板编辑状态
     */
	private String editStatus;
	
    /**
     * 模板代码
     */
	private String printCode;
	
    /**
     * 打印模板所属空间
     */
	private String printNamespace;
	
    /**
     * 打印模板内容
     */
	private String printTemplate;
	
    /**
     * 打印模板名称
     */
	private String printTemplateName;
	
    /**
     * 使用次数
     */
	private Integer useCount;
	


	public String getEditStatus() {
		return editStatus;
	}

	public void setEditStatus(String editStatus) {
		this.editStatus = editStatus;
	}

	public String getPrintCode() {
		return printCode;
	}

	public void setPrintCode(String printCode) {
		this.printCode = printCode;
	}

	public String getPrintNamespace() {
		return printNamespace;
	}

	public void setPrintNamespace(String printNamespace) {
		this.printNamespace = printNamespace;
	}

	public String getPrintTemplate() {
		return printTemplate;
	}

	public void setPrintTemplate(String printTemplate) {
		this.printTemplate = printTemplate;
	}

	public String getPrintTemplateName() {
		return printTemplateName;
	}

	public void setPrintTemplateName(String printTemplateName) {
		this.printTemplateName = printTemplateName;
	}

	public Integer getUseCount() {
		return useCount;
	}

	public void setUseCount(Integer useCount) {
		this.useCount = useCount;
	}

}
