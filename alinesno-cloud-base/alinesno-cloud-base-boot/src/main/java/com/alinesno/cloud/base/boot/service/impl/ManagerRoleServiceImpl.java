package com.alinesno.cloud.base.boot.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.alinesno.cloud.base.boot.entity.ManagerAccountEntity;
import com.alinesno.cloud.base.boot.entity.ManagerAccountRoleEntity;
import com.alinesno.cloud.base.boot.entity.ManagerRoleEntity;
import com.alinesno.cloud.base.boot.entity.ManagerRoleResourceEntity;
import com.alinesno.cloud.base.boot.repository.ManagerRoleRepository;
import com.alinesno.cloud.base.boot.service.IManagerAccountRoleService;
import com.alinesno.cloud.base.boot.service.IManagerRoleResourceService;
import com.alinesno.cloud.base.boot.service.IManagerRoleService;
import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Service
public class ManagerRoleServiceImpl extends IBaseServiceImpl<ManagerRoleRepository, ManagerRoleEntity, String> implements IManagerRoleService {
	
	//日志记录
	private static final Logger log = LoggerFactory.getLogger(ManagerRoleServiceImpl.class);

	@Autowired
	private IManagerRoleResourceService managerRoleResourceService ; 
	
	@Autowired
	private IManagerAccountRoleService managerAccountRoleService ; 
	
	@Transactional
	@Override
	public boolean saveRole(ManagerRoleEntity e , String functionIds) {
		Assert.hasLength(functionIds , "角色功能不为空.") ; 
		
		// 保存角色
		e = jpa.save(e) ; 
		
		// 保存角色资源
		managerRoleResourceService.deleteByRoleId(e.getId()); ; 
		
		String[] functionArr = functionIds.split(",") ; 
		List<ManagerRoleResourceEntity> rrList = new ArrayList<ManagerRoleResourceEntity>() ; 
		for(String f : functionArr) {
			ManagerRoleResourceEntity rr = new ManagerRoleResourceEntity() ; 
			rr.setRoleId(e.getId());
			rr.setApplicationId(e.getApplicationId());
			rr.setResourceId(f);
			rrList.add(rr) ; 
		}
		
		managerRoleResourceService.saveAll(rrList) ; 
		return true ;
	}
	
	@Transactional
	@Override
	public void deleteByIds(String[] ids) {
		
		// 1. 删除角色
		super.deleteByIds(ids);
	
		// 2. 删除角色的权限
		for(String roleId : ids) {
			managerRoleResourceService.deleteByRoleId(roleId); 
		}
	}

	@Transactional
	@Override
	public boolean authAccount(ManagerAccountEntity accountEntity, String rolesId) {
		
		log.debug("rolesId:{} , accountEntity:{}" , rolesId , accountEntity);
		
		Assert.hasLength(rolesId , "角色不为空.") ; 
		Assert.notNull(accountEntity , "用户不为空.") ; 
		
		String[] roleArr = rolesId.split(",") ; 
		// 删除用户之前的角色
		managerAccountRoleService.deleteByAccountId(accountEntity.getId()) ; 
		
		// 添加新角色
		List<ManagerAccountRoleEntity> entities = new ArrayList<ManagerAccountRoleEntity>() ; 
		for(String r : roleArr) {
			
			ManagerAccountRoleEntity e = new ManagerAccountRoleEntity() ; 
			e.setRoleId(r);
			e.setAccountId(accountEntity.getId());
			
			entities.add(e) ; 
		}
		
		managerAccountRoleService.saveAll(entities) ; 
		return true ;
	}

	@Override
	public List<ManagerRoleEntity> findByAccountId(String accountId) {
		List<ManagerAccountRoleEntity> accountRoles = managerAccountRoleService.findAllByAccountId(accountId) ; 
		List<String> roleIds = new ArrayList<String>() ; 
		for(ManagerAccountRoleEntity m : accountRoles) {
			roleIds.add(m.getRoleId()) ; 
		}
		return findAllById(roleIds);
	}


}
