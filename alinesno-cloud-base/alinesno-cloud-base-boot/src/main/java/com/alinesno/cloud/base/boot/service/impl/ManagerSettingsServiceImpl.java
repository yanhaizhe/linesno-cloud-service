package com.alinesno.cloud.base.boot.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.base.boot.entity.ManagerSettingsEntity;
import com.alinesno.cloud.base.boot.repository.ManagerSettingsRepository;
import com.alinesno.cloud.base.boot.service.IManagerSettingsService;
import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;

/**
 * <p> 参数配置表 服务实现类 </p>
 *
 * @author WeiXiaoJin
 * @since 2019-07-06 15:47:49
 */
@Service
public class ManagerSettingsServiceImpl extends IBaseServiceImpl<ManagerSettingsRepository, ManagerSettingsEntity, String> implements IManagerSettingsService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(ManagerSettingsServiceImpl.class);

}
