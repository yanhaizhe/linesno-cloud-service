package com.alinesno.cloud.base.boot.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alinesno.cloud.base.boot.entity.ManagerSettingsEntity;
import com.alinesno.cloud.base.boot.service.IManagerSettingsService;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.core.rest.BaseRestController;

/**
 * <p>参数配置表 接口 </p>
 *
 * @author WeiXiaoJin
 * @since 2019-07-06 15:47:49
 */
@Scope(SpringInstanceScope.PROTOTYPE)
@RestController
@RequestMapping("managerSettings")
public class ManagerSettingsRestController extends BaseRestController<ManagerSettingsEntity , IManagerSettingsService> {

	//日志记录
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(ManagerSettingsRestController.class);

}
