package com.alinesno.cloud.base.boot.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Entity
@Table(name="info_code")
public class InfoCodeEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

	private String owners;
	@Column(name="code_name")
	private String codeName;
	@Column(name="code_type")
	private String codeType;
	@Column(name="code_value")
	private String codeValue;


	public String getOwners() {
		return owners;
	}

	public void setOwners(String owners) {
		this.owners = owners;
	}

	public String getCodeName() {
		return codeName;
	}

	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}

	public String getCodeType() {
		return codeType;
	}

	public void setCodeType(String codeType) {
		this.codeType = codeType;
	}

	public String getCodeValue() {
		return codeValue;
	}

	public void setCodeValue(String codeValue) {
		this.codeValue = codeValue;
	}


	@Override
	public String toString() {
		return "InfoCodeEntity{" +
			"owners=" + owners +
			", codeName=" + codeName +
			", codeType=" + codeType +
			", codeValue=" + codeValue +
			"}";
	}
}
