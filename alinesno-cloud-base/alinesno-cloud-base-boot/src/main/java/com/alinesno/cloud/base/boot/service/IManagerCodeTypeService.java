package com.alinesno.cloud.base.boot.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.base.boot.entity.ManagerCodeTypeEntity;
import com.alinesno.cloud.base.boot.repository.ManagerCodeTypeRepository;
import com.alinesno.cloud.common.core.services.IBaseService;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-02-07 21:16:11
 */
@NoRepositoryBean
public interface IManagerCodeTypeService extends IBaseService<ManagerCodeTypeRepository, ManagerCodeTypeEntity, String> {

	ManagerCodeTypeEntity findByCodeTypeValue(String codeTypeValue);

}
